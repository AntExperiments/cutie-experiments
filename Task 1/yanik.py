import time, re, sys
beginTime = time.time()

lineArray = open(input("Filename to scan: "), 'r').read().split(",")
startCalcTime = time.time()

# remove last line from file and store it for output
lastValue = lineArray.pop()
output = 0.00;

for element in lineArray:
    if re.match(r"^(\n)?(\-|\+)?[0-9]+(\.[0-9]+)$", element):
        output += float(element)
    else:
        sys.exit("Non-valid input detected:" + element)

print(f"{len(lineArray)} lines processed in {round(time.time() - startCalcTime, 2)} (out of {round(time.time() - beginTime, 2)}) seconds")
print(f"Calculated last value: {output}")
print(f"Correct last value: {lastValue}")